import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/app_theme.dart';
import 'package:flutter_core/di.dart';
import 'package:flutter_core/photo_gallery/domain/gallery_cubit.dart';
import 'package:flutter_core/photo_gallery/presentation/gallery_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() async {
  await initGetIt();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<GalleryCubit>(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: AppTheme.lightTheme,
        darkTheme: AppTheme.darkTheme,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text(AppLocalizations.of(context).helloWorld),
    ),
    body: GalleryWidget(),
  );
}
