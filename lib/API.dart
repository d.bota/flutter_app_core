class PlaceholderApi {
  PlaceholderApi._();

  static const API_URL = "https://jsonplaceholder.typicode.com/";
  static const ALBUMS_ENDPOINT = API_URL + "albums";
  static const PICTURES_ENDPOINT = API_URL + "pictures";
}