import 'package:dio/dio.dart';

enum NetworkErrorKind {
  TIMEOUT, SERVER, CLIENT, CANCELLED, OTHER
}

class NetworkException implements Exception {
  final NetworkErrorKind errorKind;

  NetworkException._(this.errorKind);

  factory NetworkException.fromDioError(DioError dioError) {
    switch (dioError.type) {
      case DioErrorType.CONNECT_TIMEOUT:
      case DioErrorType.SEND_TIMEOUT:
      case DioErrorType.RECEIVE_TIMEOUT:
        return NetworkException._(NetworkErrorKind.TIMEOUT);
        break;
      case DioErrorType.CANCEL:
        return NetworkException._(NetworkErrorKind.CANCELLED);
        break;
      case DioErrorType.RESPONSE:
        return _fromResponse(dioError);
        break;
      case DioErrorType.DEFAULT:
        return NetworkException._(NetworkErrorKind.OTHER);
        break;
      default: throw Exception("unreachable");
    }
  }

  static NetworkException _fromResponse(DioError dioError) {
    if (dioError.response.statusCode >= 500) {
      return NetworkException._(NetworkErrorKind.SERVER);
    } else {
      return NetworkException._(NetworkErrorKind.CLIENT);
    }
  }
}