import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_core/common/exceptions/network_exception.dart';

class NetworkErrorWidget extends StatelessWidget {
  final NetworkException _exception;
  final VoidCallback _retryCallback;

  NetworkErrorWidget(this._exception, this._retryCallback);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.error_outline),
          Text(_getLocalizedMessage(context)),
          FlatButton(onPressed: () => _retryCallback(), child: Text("retry"))
        ],
      ),
    );
  }

  String _getLocalizedMessage(BuildContext context) {
    // TODO, use build context to get localized errors
    String message;
    switch (_exception.errorKind) {
      case NetworkErrorKind.TIMEOUT:
        message = "timeout";
        break;
      case NetworkErrorKind.SERVER:
        message = "server problem";
        break;
      case NetworkErrorKind.CLIENT:
        message = "client problem";
        break;
      case NetworkErrorKind.CANCELLED:
        message = "request cancelled";
        break;
      case NetworkErrorKind.OTHER:
        message = "other";
        break;
    }

    return message;
  }
}
