import 'package:dio/dio.dart';
import 'package:flutter_core/photo_gallery/data/gallery_repository.dart';
import 'package:flutter_core/photo_gallery/data/network/gallery_data_source.dart';
import 'package:flutter_core/photo_gallery/domain/gallery_cubit.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
Future<void> initGetIt() async {
  getIt.registerSingleton<Dio>(Dio());
  getIt.registerFactory<GalleryDataSource>(() => GalleryDataSource(getIt()));
  getIt.registerFactory<GalleryRepository>(() => GalleryRepository(getIt()));
  getIt.registerFactory<GalleryCubit>(() => GalleryCubit(getIt()));

  return getIt.allReady();
}