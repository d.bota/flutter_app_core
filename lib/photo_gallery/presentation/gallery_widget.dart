import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/common/widgets/network_error_widget.dart';
import 'package:flutter_core/photo_gallery/domain/entity/album.dart';
import 'package:flutter_core/photo_gallery/domain/gallery_cubit.dart';
import 'package:flutter_core/photo_gallery/domain/gallery_viewstate.dart';

typedef void AlbumClicked(Album album);

class GalleryWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cubit = context.read<GalleryCubit>();

    return BlocConsumer<GalleryCubit, GalleryViewState>(
      cubit: cubit,
      listener: (context, state) {
        state.maybeWhen(
          unknownError: (e) => Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text("unknown error"))),
          orElse: () {},
        );
      },
      builder: (context, state) => state.when(
        progress: () => Center(child: CircularProgressIndicator()),
        unknownError: (e) => Container(),
        networkError: (e) => NetworkErrorWidget(e, () => cubit.fetchAlbums()),
        data: (albums) => ListView.separated(
          itemCount: albums.length,
          separatorBuilder: (context, position) => Divider(),
          itemBuilder: (context, position) => _AlbumTile(
            albums[position],
            (album) => cubit.onAlbumClicked(album),
          ),
        ),
      ),
    );
  }
}

class _AlbumTile extends StatelessWidget {
  final Album _album;
  final AlbumClicked _onClicked;

  _AlbumTile(this._album, this._onClicked);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => _onClicked(_album),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Icon(Icons.album),
              Text(_album.title),
            ],
          ),
        ),
      );
}
