// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'gallery_viewstate.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$GalleryViewStateTearOff {
  const _$GalleryViewStateTearOff();

// ignore: unused_element
  Progress progress() {
    return const Progress();
  }

// ignore: unused_element
  Data data(List<Album> album) {
    return Data(
      album,
    );
  }

// ignore: unused_element
  NetworkError networkError(NetworkException exception) {
    return NetworkError(
      exception,
    );
  }

// ignore: unused_element
  UnknownError unknownError(UnknownException exception) {
    return UnknownError(
      exception,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $GalleryViewState = _$GalleryViewStateTearOff();

/// @nodoc
mixin _$GalleryViewState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult progress(),
    @required TResult data(List<Album> album),
    @required TResult networkError(NetworkException exception),
    @required TResult unknownError(UnknownException exception),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult progress(),
    TResult data(List<Album> album),
    TResult networkError(NetworkException exception),
    TResult unknownError(UnknownException exception),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult progress(Progress value),
    @required TResult data(Data value),
    @required TResult networkError(NetworkError value),
    @required TResult unknownError(UnknownError value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult progress(Progress value),
    TResult data(Data value),
    TResult networkError(NetworkError value),
    TResult unknownError(UnknownError value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $GalleryViewStateCopyWith<$Res> {
  factory $GalleryViewStateCopyWith(
          GalleryViewState value, $Res Function(GalleryViewState) then) =
      _$GalleryViewStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GalleryViewStateCopyWithImpl<$Res>
    implements $GalleryViewStateCopyWith<$Res> {
  _$GalleryViewStateCopyWithImpl(this._value, this._then);

  final GalleryViewState _value;
  // ignore: unused_field
  final $Res Function(GalleryViewState) _then;
}

/// @nodoc
abstract class $ProgressCopyWith<$Res> {
  factory $ProgressCopyWith(Progress value, $Res Function(Progress) then) =
      _$ProgressCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProgressCopyWithImpl<$Res> extends _$GalleryViewStateCopyWithImpl<$Res>
    implements $ProgressCopyWith<$Res> {
  _$ProgressCopyWithImpl(Progress _value, $Res Function(Progress) _then)
      : super(_value, (v) => _then(v as Progress));

  @override
  Progress get _value => super._value as Progress;
}

/// @nodoc
class _$Progress implements Progress {
  const _$Progress();

  @override
  String toString() {
    return 'GalleryViewState.progress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Progress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult progress(),
    @required TResult data(List<Album> album),
    @required TResult networkError(NetworkException exception),
    @required TResult unknownError(UnknownException exception),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return progress();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult progress(),
    TResult data(List<Album> album),
    TResult networkError(NetworkException exception),
    TResult unknownError(UnknownException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (progress != null) {
      return progress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult progress(Progress value),
    @required TResult data(Data value),
    @required TResult networkError(NetworkError value),
    @required TResult unknownError(UnknownError value),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return progress(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult progress(Progress value),
    TResult data(Data value),
    TResult networkError(NetworkError value),
    TResult unknownError(UnknownError value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (progress != null) {
      return progress(this);
    }
    return orElse();
  }
}

abstract class Progress implements GalleryViewState {
  const factory Progress() = _$Progress;
}

/// @nodoc
abstract class $DataCopyWith<$Res> {
  factory $DataCopyWith(Data value, $Res Function(Data) then) =
      _$DataCopyWithImpl<$Res>;
  $Res call({List<Album> album});
}

/// @nodoc
class _$DataCopyWithImpl<$Res> extends _$GalleryViewStateCopyWithImpl<$Res>
    implements $DataCopyWith<$Res> {
  _$DataCopyWithImpl(Data _value, $Res Function(Data) _then)
      : super(_value, (v) => _then(v as Data));

  @override
  Data get _value => super._value as Data;

  @override
  $Res call({
    Object album = freezed,
  }) {
    return _then(Data(
      album == freezed ? _value.album : album as List<Album>,
    ));
  }
}

/// @nodoc
class _$Data implements Data {
  const _$Data(this.album) : assert(album != null);

  @override
  final List<Album> album;

  @override
  String toString() {
    return 'GalleryViewState.data(album: $album)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Data &&
            (identical(other.album, album) ||
                const DeepCollectionEquality().equals(other.album, album)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(album);

  @JsonKey(ignore: true)
  @override
  $DataCopyWith<Data> get copyWith =>
      _$DataCopyWithImpl<Data>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult progress(),
    @required TResult data(List<Album> album),
    @required TResult networkError(NetworkException exception),
    @required TResult unknownError(UnknownException exception),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return data(album);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult progress(),
    TResult data(List<Album> album),
    TResult networkError(NetworkException exception),
    TResult unknownError(UnknownException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (data != null) {
      return data(album);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult progress(Progress value),
    @required TResult data(Data value),
    @required TResult networkError(NetworkError value),
    @required TResult unknownError(UnknownError value),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult progress(Progress value),
    TResult data(Data value),
    TResult networkError(NetworkError value),
    TResult unknownError(UnknownError value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class Data implements GalleryViewState {
  const factory Data(List<Album> album) = _$Data;

  List<Album> get album;
  @JsonKey(ignore: true)
  $DataCopyWith<Data> get copyWith;
}

/// @nodoc
abstract class $NetworkErrorCopyWith<$Res> {
  factory $NetworkErrorCopyWith(
          NetworkError value, $Res Function(NetworkError) then) =
      _$NetworkErrorCopyWithImpl<$Res>;
  $Res call({NetworkException exception});
}

/// @nodoc
class _$NetworkErrorCopyWithImpl<$Res>
    extends _$GalleryViewStateCopyWithImpl<$Res>
    implements $NetworkErrorCopyWith<$Res> {
  _$NetworkErrorCopyWithImpl(
      NetworkError _value, $Res Function(NetworkError) _then)
      : super(_value, (v) => _then(v as NetworkError));

  @override
  NetworkError get _value => super._value as NetworkError;

  @override
  $Res call({
    Object exception = freezed,
  }) {
    return _then(NetworkError(
      exception == freezed ? _value.exception : exception as NetworkException,
    ));
  }
}

/// @nodoc
class _$NetworkError implements NetworkError {
  const _$NetworkError(this.exception) : assert(exception != null);

  @override
  final NetworkException exception;

  @override
  String toString() {
    return 'GalleryViewState.networkError(exception: $exception)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NetworkError &&
            (identical(other.exception, exception) ||
                const DeepCollectionEquality()
                    .equals(other.exception, exception)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(exception);

  @JsonKey(ignore: true)
  @override
  $NetworkErrorCopyWith<NetworkError> get copyWith =>
      _$NetworkErrorCopyWithImpl<NetworkError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult progress(),
    @required TResult data(List<Album> album),
    @required TResult networkError(NetworkException exception),
    @required TResult unknownError(UnknownException exception),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return networkError(exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult progress(),
    TResult data(List<Album> album),
    TResult networkError(NetworkException exception),
    TResult unknownError(UnknownException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (networkError != null) {
      return networkError(exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult progress(Progress value),
    @required TResult data(Data value),
    @required TResult networkError(NetworkError value),
    @required TResult unknownError(UnknownError value),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return networkError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult progress(Progress value),
    TResult data(Data value),
    TResult networkError(NetworkError value),
    TResult unknownError(UnknownError value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (networkError != null) {
      return networkError(this);
    }
    return orElse();
  }
}

abstract class NetworkError implements GalleryViewState {
  const factory NetworkError(NetworkException exception) = _$NetworkError;

  NetworkException get exception;
  @JsonKey(ignore: true)
  $NetworkErrorCopyWith<NetworkError> get copyWith;
}

/// @nodoc
abstract class $UnknownErrorCopyWith<$Res> {
  factory $UnknownErrorCopyWith(
          UnknownError value, $Res Function(UnknownError) then) =
      _$UnknownErrorCopyWithImpl<$Res>;
  $Res call({UnknownException exception});
}

/// @nodoc
class _$UnknownErrorCopyWithImpl<$Res>
    extends _$GalleryViewStateCopyWithImpl<$Res>
    implements $UnknownErrorCopyWith<$Res> {
  _$UnknownErrorCopyWithImpl(
      UnknownError _value, $Res Function(UnknownError) _then)
      : super(_value, (v) => _then(v as UnknownError));

  @override
  UnknownError get _value => super._value as UnknownError;

  @override
  $Res call({
    Object exception = freezed,
  }) {
    return _then(UnknownError(
      exception == freezed ? _value.exception : exception as UnknownException,
    ));
  }
}

/// @nodoc
class _$UnknownError implements UnknownError {
  const _$UnknownError(this.exception) : assert(exception != null);

  @override
  final UnknownException exception;

  @override
  String toString() {
    return 'GalleryViewState.unknownError(exception: $exception)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UnknownError &&
            (identical(other.exception, exception) ||
                const DeepCollectionEquality()
                    .equals(other.exception, exception)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(exception);

  @JsonKey(ignore: true)
  @override
  $UnknownErrorCopyWith<UnknownError> get copyWith =>
      _$UnknownErrorCopyWithImpl<UnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult progress(),
    @required TResult data(List<Album> album),
    @required TResult networkError(NetworkException exception),
    @required TResult unknownError(UnknownException exception),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return unknownError(exception);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult progress(),
    TResult data(List<Album> album),
    TResult networkError(NetworkException exception),
    TResult unknownError(UnknownException exception),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unknownError != null) {
      return unknownError(exception);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult progress(Progress value),
    @required TResult data(Data value),
    @required TResult networkError(NetworkError value),
    @required TResult unknownError(UnknownError value),
  }) {
    assert(progress != null);
    assert(data != null);
    assert(networkError != null);
    assert(unknownError != null);
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult progress(Progress value),
    TResult data(Data value),
    TResult networkError(NetworkError value),
    TResult unknownError(UnknownError value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class UnknownError implements GalleryViewState {
  const factory UnknownError(UnknownException exception) = _$UnknownError;

  UnknownException get exception;
  @JsonKey(ignore: true)
  $UnknownErrorCopyWith<UnknownError> get copyWith;
}
