import 'package:freezed_annotation/freezed_annotation.dart';

part 'photo.freezed.dart';

@freezed
abstract class Photo with _$Photo {
  const factory Photo(
    int id,
    String title,
    String url,
    String thumbnail,
  ) = _Photo;
}
