// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'photo.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PhotoTearOff {
  const _$PhotoTearOff();

// ignore: unused_element
  _Photo call(int id, String title, String url, String thumbnail) {
    return _Photo(
      id,
      title,
      url,
      thumbnail,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Photo = _$PhotoTearOff();

/// @nodoc
mixin _$Photo {
  int get id;
  String get title;
  String get url;
  String get thumbnail;

  @JsonKey(ignore: true)
  $PhotoCopyWith<Photo> get copyWith;
}

/// @nodoc
abstract class $PhotoCopyWith<$Res> {
  factory $PhotoCopyWith(Photo value, $Res Function(Photo) then) =
      _$PhotoCopyWithImpl<$Res>;
  $Res call({int id, String title, String url, String thumbnail});
}

/// @nodoc
class _$PhotoCopyWithImpl<$Res> implements $PhotoCopyWith<$Res> {
  _$PhotoCopyWithImpl(this._value, this._then);

  final Photo _value;
  // ignore: unused_field
  final $Res Function(Photo) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object url = freezed,
    Object thumbnail = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as int,
      title: title == freezed ? _value.title : title as String,
      url: url == freezed ? _value.url : url as String,
      thumbnail: thumbnail == freezed ? _value.thumbnail : thumbnail as String,
    ));
  }
}

/// @nodoc
abstract class _$PhotoCopyWith<$Res> implements $PhotoCopyWith<$Res> {
  factory _$PhotoCopyWith(_Photo value, $Res Function(_Photo) then) =
      __$PhotoCopyWithImpl<$Res>;
  @override
  $Res call({int id, String title, String url, String thumbnail});
}

/// @nodoc
class __$PhotoCopyWithImpl<$Res> extends _$PhotoCopyWithImpl<$Res>
    implements _$PhotoCopyWith<$Res> {
  __$PhotoCopyWithImpl(_Photo _value, $Res Function(_Photo) _then)
      : super(_value, (v) => _then(v as _Photo));

  @override
  _Photo get _value => super._value as _Photo;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object url = freezed,
    Object thumbnail = freezed,
  }) {
    return _then(_Photo(
      id == freezed ? _value.id : id as int,
      title == freezed ? _value.title : title as String,
      url == freezed ? _value.url : url as String,
      thumbnail == freezed ? _value.thumbnail : thumbnail as String,
    ));
  }
}

/// @nodoc
class _$_Photo implements _Photo {
  const _$_Photo(this.id, this.title, this.url, this.thumbnail)
      : assert(id != null),
        assert(title != null),
        assert(url != null),
        assert(thumbnail != null);

  @override
  final int id;
  @override
  final String title;
  @override
  final String url;
  @override
  final String thumbnail;

  @override
  String toString() {
    return 'Photo(id: $id, title: $title, url: $url, thumbnail: $thumbnail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Photo &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.thumbnail, thumbnail) ||
                const DeepCollectionEquality()
                    .equals(other.thumbnail, thumbnail)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(thumbnail);

  @JsonKey(ignore: true)
  @override
  _$PhotoCopyWith<_Photo> get copyWith =>
      __$PhotoCopyWithImpl<_Photo>(this, _$identity);
}

abstract class _Photo implements Photo {
  const factory _Photo(int id, String title, String url, String thumbnail) =
      _$_Photo;

  @override
  int get id;
  @override
  String get title;
  @override
  String get url;
  @override
  String get thumbnail;
  @override
  @JsonKey(ignore: true)
  _$PhotoCopyWith<_Photo> get copyWith;
}
