import 'package:bloc/bloc.dart';
import 'package:flutter_core/common/exceptions/network_exception.dart';
import 'package:flutter_core/common/exceptions/unknown_exception.dart';
import 'package:flutter_core/photo_gallery/data/gallery_repository.dart';
import 'package:flutter_core/photo_gallery/domain/entity/album.dart';
import 'package:flutter_core/photo_gallery/domain/gallery_viewstate.dart';

class GalleryCubit extends Cubit<GalleryViewState> {
  final GalleryRepository _repository;

  GalleryCubit(this._repository) : super(GalleryViewState.progress()) {
    fetchAlbums();
  }

  void fetchAlbums() async {
    try {
      final albums = await _repository.getAlbums();
      emit(GalleryViewState.data(albums));
    } on NetworkException catch(e) {
      emit(GalleryViewState.networkError(e));
    } on UnknownException catch(e) {
      final currentState = state;
      emit(GalleryViewState.unknownError(e));
      emit(currentState);
    }
  }

  void onAlbumClicked(Album album) {

  }
}
