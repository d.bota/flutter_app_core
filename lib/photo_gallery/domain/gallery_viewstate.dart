import 'package:flutter_core/common/exceptions/network_exception.dart';
import 'package:flutter_core/common/exceptions/unknown_exception.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'entity/album.dart';

part 'gallery_viewstate.freezed.dart';

@freezed
abstract class GalleryViewState with _$GalleryViewState {
  const factory GalleryViewState.progress() = Progress;
  const factory GalleryViewState.data(List<Album> album) = Data;
  const factory GalleryViewState.networkError(NetworkException exception) = NetworkError;
  const factory GalleryViewState.unknownError(UnknownException exception) = UnknownError;
}
