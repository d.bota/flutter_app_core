import 'package:dio/dio.dart';
import 'package:flutter_core/API.dart';

import 'entity/album_response.dart';
import 'entity/photos_response.dart';

class GalleryDataSource {
  final Dio _dio;

  GalleryDataSource(this._dio);

  Future<Iterable<AlbumResponse>> getAlbums() async {
    final answer = await _dio.get(PlaceholderApi.ALBUMS_ENDPOINT);

    return (answer.data as List)
        .map((album) => AlbumResponse.fromJson(album));
  }

  Future<Iterable<PhotoResponse>> getPhotos(int albumId) async {
    final answer =
        await _dio.get("${PlaceholderApi.PICTURES_ENDPOINT}/$albumId");

    return (answer.data as List)
        .map((photo) => PhotoResponse.fromJson(photo))
        .where((PhotoResponse photo) => photo.albumId == albumId);
  }
}
