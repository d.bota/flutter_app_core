import 'package:json_annotation/json_annotation.dart';

part 'photos_response.g.dart';

@JsonSerializable(explicitToJson: true)
class PhotoResponse {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  PhotoResponse(this.albumId, this.id, this.title, this.url, this.thumbnailUrl);

  factory PhotoResponse.fromJson(Map<String, dynamic> json) => _$PhotoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoResponseToJson(this);
}