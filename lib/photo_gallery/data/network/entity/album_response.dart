import 'package:json_annotation/json_annotation.dart';

part 'album_response.g.dart';

@JsonSerializable(explicitToJson: true)
class AlbumResponse {
  final int userId;
  final int id;
  final String title;

  AlbumResponse(this.userId, this.id, this.title);

  factory AlbumResponse.fromJson(Map<String, dynamic> json) => _$AlbumResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AlbumResponseToJson(this);
}