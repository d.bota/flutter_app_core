import 'package:dio/dio.dart';
import 'package:flutter_core/common/exceptions/network_exception.dart';
import 'package:flutter_core/common/exceptions/unknown_exception.dart';
import 'package:flutter_core/photo_gallery/data/network/gallery_data_source.dart';
import 'package:flutter_core/photo_gallery/domain/entity/album.dart';
import 'package:flutter_core/photo_gallery/domain/entity/photo.dart';

import 'network/entity/album_response.dart';
import 'network/entity/photos_response.dart';

class GalleryRepository {
  final GalleryDataSource _dataSource;

  GalleryRepository(this._dataSource);

  Future<List<Album>> getAlbums() async {
    Iterable<AlbumResponse> albums;
    try {
      albums = await _dataSource.getAlbums();
    } on DioError catch (e) {
      throw NetworkException.fromDioError(e);
    } catch (e) {
      throw UnknownException();
    }

    return albums
        .map((albumResponse) => Album(albumResponse.id, albumResponse.title))
        .toList();
  }

  Future<List<Photo>> getPhotos(int albumId) async {
    Iterable<PhotoResponse> photos;
    try {
      photos = await _dataSource.getPhotos(albumId);
    } on DioError catch (e) {
      throw NetworkException.fromDioError(e);
    } catch (e) {
      throw UnknownException();
    }

    return photos
        .map((p) => Photo(p.id, p.title, p.url, p.thumbnailUrl))
        .toList();
  }
}
